package com.example.hw02;

import static com.example.hw02.Constants.DATE_FORMAT;
import static com.example.hw02.Constants.TIME_FORMAT;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * BaseActivity.java
 */
 
public class BaseActivity extends Activity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
	
	protected Calendar dateChosen;
	protected Calendar timeChosen;
	protected EditText titleText;
	protected EditText dateText;
	protected EditText timeText;
	protected RadioGroup priorityGroup;
	protected Button updateTaskButton;
	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		Log.d("demo", "Hour I received from time picker " + hourOfDay);
		timeChosen = Calendar.getInstance();
		timeChosen.set(Calendar.HOUR_OF_DAY, hourOfDay);
		timeChosen.set(Calendar.MINUTE, minute);
		SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
		timeText.setText(sdf.format(timeChosen.getTime()));
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		dateChosen = Calendar.getInstance();
		dateChosen.set(year, monthOfYear, dayOfMonth);
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		dateText.setText(sdf.format(dateChosen.getTime()));
	}

	protected String validateFields() {
		String errorMessage = "";
		if(titleText.getText().toString().length() == 0)
			errorMessage += "Title Field empty\n";
		if(dateChosen == null)
			errorMessage += "Date field cannot be blank \n";
		if(timeChosen == null)
			errorMessage += "Time field cannot be blank";
		
		return errorMessage;
	}
}
