package com.example.hw02;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * MyTimePicker.java
 */

public class MyTimePicker extends DialogFragment {

	TimePickerDialog.OnTimeSetListener listener;
	Calendar cal;
	
	public MyTimePicker(TimePickerDialog.OnTimeSetListener listener) {
		this.listener = listener;
		this.cal = Calendar.getInstance();
	}
	
	public MyTimePicker(TimePickerDialog.OnTimeSetListener listener, Calendar cal) {
		this.listener = listener;
		this.cal = cal;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setCanceledOnTouchOutside(false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new TimePickerDialog(getActivity(), listener,
				cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false);
	}
}
