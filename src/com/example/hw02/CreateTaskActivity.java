package com.example.hw02;

import static com.example.hw02.Constants.ACTION;
import static com.example.hw02.Constants.ACTION_CREATE;
import static com.example.hw02.Constants.TASK;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * CreateTaskActivity.java
 */

public class CreateTaskActivity extends BaseActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_task);

		titleText = (EditText) findViewById(R.id.titleEditText);
		dateText = (EditText) findViewById(R.id.dateEditText);
		timeText = (EditText) findViewById(R.id.timeEditText);
		
		dateChosen = null;
		timeChosen = null;
		
		priorityGroup = (RadioGroup) findViewById(R.id.priorityRadioGroup);
		
		dateText.setKeyListener(null);
		
		dateText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				MyDatePicker datePicker = new MyDatePicker(CreateTaskActivity.this);
				datePicker.show(getFragmentManager(), "Pick Date");
			}
		});
		
		timeText.setKeyListener(null);
		
		timeText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MyTimePicker timePicker = new MyTimePicker(CreateTaskActivity.this);
				timePicker.show(getFragmentManager(), "Set Time");
			}
		});
		
		Button updateTaskButton = (Button) findViewById(R.id.updateTaskButton);
		updateTaskButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String errors;
				if((errors = validateFields()).length() == 0) {
					RadioButton priority = (RadioButton) findViewById(priorityGroup.getCheckedRadioButtonId());
					Intent intent = new Intent();
					Task newTask = new Task(titleText.getText().toString(), dateChosen, timeChosen, priority.getText().toString());
					intent.putExtra(TASK, newTask);
					intent.putExtra(ACTION, ACTION_CREATE);
					setResult(RESULT_OK, intent);
					finish();
				} else {
					Toast.makeText(CreateTaskActivity.this, errors, Toast.LENGTH_LONG).show();
				}
			}
		});

	}
	
}
