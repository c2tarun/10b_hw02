package com.example.hw02;

import static com.example.hw02.Constants.ACTION;
import static com.example.hw02.Constants.ACTION_DISCARD;
import static com.example.hw02.Constants.ACTION_UPDATE;
import static com.example.hw02.Constants.CREATE_TASK;
import static com.example.hw02.Constants.DATE_FORMAT;
import static com.example.hw02.Constants.DISPLAY_TASK;
import static com.example.hw02.Constants.OLD_TASK;
import static com.example.hw02.Constants.TASK;
import static com.example.hw02.Constants.TIME_FORMAT;

import java.text.SimpleDateFormat;
import java.util.LinkedList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * MainActivity.java
 */

public class MainActivity extends Activity {

	LinkedList<Task> taskList;
	LinearLayout taskListLayout;
	ImageButton addNewTask;
	TextView statusTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		taskList = new LinkedList<Task>();
		taskListLayout = (LinearLayout) findViewById(R.id.taskListLayout);
		addNewTask = (ImageButton) findViewById(R.id.newTaskButton);

		statusTextView = (TextView) findViewById(R.id.statusTextView);

		addNewTask.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,
						CreateTaskActivity.class);
				startActivityForResult(intent, CREATE_TASK);
			}
		});
		updateStatus();
	}

	public void updateStatus() {
		int totalTasks = taskList.size();
		statusTextView.setText(totalTasks + " Tasks");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && data.getExtras() != null) {
			String action = (String) data.getExtras().get(ACTION);
			if (requestCode == CREATE_TASK) {
				Task newTask = (Task) data.getExtras().get(TASK);
				addTaskToList(newTask);
			} else if (requestCode == DISPLAY_TASK && ACTION_DISCARD.equals(action)) {
				Task taskToDiscard = (Task) data.getExtras().get(TASK);
				removeTaskFromList(taskToDiscard);
			} else if(requestCode == DISPLAY_TASK && ACTION_UPDATE.equals(action)) {
				Task taskToUpdate = (Task) data.getExtras().get(TASK);
				Task taskToDelete = (Task) data.getExtras().get(OLD_TASK);
//				removeTaskFromList(taskToDelete);
//				addTaskToList(taskToUpdate);
				updateTaskInList(taskToDelete, taskToUpdate);
			}
			updateTaskListLayout();
		}
		updateStatus();
	}
	
	private void addTaskToList(Task task) {
		Log.d("demo", "Task to add " + task.getTaskId());
		taskList.addFirst(task);
	}
	
	private void removeTaskFromList(Task task) {
		Log.d("demo", "Task to delete " + task.getTaskId());
		taskList.remove(task);
	}
	
	private void updateTaskInList(Task oldTask, Task newTask) {
		int index = taskList.indexOf(oldTask);
		if(index > -1) {
			taskList.remove(index);
			taskList.add(index, newTask);
		} else {
			addTaskToList(newTask);
		}
	}

	private void updateTaskListLayout() {
		taskListLayout.removeAllViews();
		for (Task task : taskList) {
			taskListLayout.addView(createTaskLayout(task));
		}
	}

	private LinearLayout createTaskLayout(Task task) {
		LinearLayout taskLayout = new LinearLayout(this);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

		layoutParams.setMargins(30, 15, 0, 0);
		taskLayout.setLayoutParams(layoutParams);
		taskLayout.setOrientation(LinearLayout.VERTICAL);

		TextView title = new TextView(this);
		title.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		title.setText(task.getTitle());
		title.setTextSize(getResources().getDimension(R.dimen.title));
		taskLayout.addView(title);
		
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		TextView date = new TextView(this);
		date.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		date.setText(sdf.format(task.getDate().getTime()));
		date.setPadding(30, 0, 0, 0);
		taskLayout.addView(date);

		sdf = new SimpleDateFormat(TIME_FORMAT);
		TextView time = new TextView(this);
		time.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		time.setText(sdf.format(task.getTime().getTime()));
		time.setPadding(30, 0, 0, 0);
		taskLayout.addView(time);

		

		taskLayout.setTag(R.id.CURRENT_TAG, task);
		taskLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Task task = (Task) v.getTag(R.id.CURRENT_TAG);
				Intent intent = new Intent(MainActivity.this,
						DisplayActivity.class);
				intent.putExtra(TASK, task);
				startActivityForResult(intent, DISPLAY_TASK);
			}
		});
		return taskLayout;
	}
}
