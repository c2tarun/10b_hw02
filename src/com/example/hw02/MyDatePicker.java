package com.example.hw02;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * MyDatePicker.java
 */

public class MyDatePicker extends DialogFragment {
	
	DatePickerDialog.OnDateSetListener listener;
	Calendar cal;
	
	public MyDatePicker(DatePickerDialog.OnDateSetListener listener) {
		// TODO Auto-generated constructor stub
		this.listener = listener;
		this.cal = Calendar.getInstance();
	}
	
	public MyDatePicker(DatePickerDialog.OnDateSetListener listener, Calendar cal) {
		this.listener = listener;
		this.cal = cal;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new DatePickerDialog(getActivity(), listener, cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setCanceledOnTouchOutside(false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

}
