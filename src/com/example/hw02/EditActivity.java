package com.example.hw02;

import static com.example.hw02.Constants.ACTION;
import static com.example.hw02.Constants.ACTION_UPDATE;
import static com.example.hw02.Constants.TASK;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * EditActivity.java
 */

public class EditActivity extends BaseActivity{

	Task task;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);

		titleText = (EditText) findViewById(R.id.titleEditText);
		dateText = (EditText) findViewById(R.id.dateEditText);
		timeText = (EditText) findViewById(R.id.timeEditText);
		updateTaskButton = (Button) findViewById(R.id.updateTaskButton);
		priorityGroup = (RadioGroup) findViewById(R.id.priorityRadioGroup);

		if(getIntent().getExtras() != null) {
			task = (Task) getIntent().getExtras().get(TASK);
			if(task != null)
				populateTaskToView(task);
		}
		
		dateText.setKeyListener(null);

		dateText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MyDatePicker datePicker = new MyDatePicker(EditActivity.this, dateChosen);
				datePicker.show(getFragmentManager(), "Pick Date");
			}
		});
		
		timeText.setKeyListener(null);

		timeText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MyTimePicker timePicker = new MyTimePicker(EditActivity.this, timeChosen);
				timePicker.show(getFragmentManager(), "Set Time");
			}
		});

		updateTaskButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String errors;
				if ((errors = validateFields()).length() == 0) {
					RadioButton priority = (RadioButton) findViewById(priorityGroup.getCheckedRadioButtonId());
					Intent intent = new Intent();
					Task newTask = new Task(titleText.getText().toString(),
							dateChosen, timeChosen, priority.getText().toString());
					intent.putExtra(TASK, newTask);
					intent.putExtra(ACTION, ACTION_UPDATE);
					setResult(RESULT_OK, intent);
					finish();
				} else {
					Toast.makeText(EditActivity.this, errors, Toast.LENGTH_LONG).show();
				}
			}
		});

	}
	
	private void populateTaskToView(Task task) {
		dateChosen = task.getDate();
		timeChosen = task.getTime();
		titleText.setText(task.getTitle());
		dateText.setText(task.getFormatedDate());
		timeText.setText(task.getFormatedTime());
		if(getResources().getString(R.string.high_priority).equals(task.getPriority())) {
			((RadioButton)findViewById(R.id.highPriorityRadio)).setChecked(true);
		}
		if(getResources().getString(R.string.medium_priority).equals(task.getPriority())) {
			((RadioButton)findViewById(R.id.mediumPriorityRadio)).setChecked(true);
		}
		if(getResources().getString(R.string.low_priority).equals(task.getPriority())) {
			((RadioButton)findViewById(R.id.lowPriorityRadio)).setChecked(true);
		}
	}

}
