package com.example.hw02;

import static com.example.hw02.Constants.*;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * DisplayActivity.java
 */

public class DisplayActivity extends Activity implements OnClickListener{
	
	TextView title;
	TextView date;
	TextView time;
	TextView priority;
	
	ImageButton editButton;
	ImageButton discardButton;
	Task task;
	Task taskToDelete;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);

		if (getIntent().getExtras() != null) {
			task = (Task) getIntent().getExtras().get(TASK);
			taskToDelete = task;
			title = (TextView) findViewById(R.id.titleTextView);
			date = (TextView) findViewById(R.id.dateTextView);
			time = (TextView) findViewById(R.id.timeTextView);
			priority = (TextView) findViewById(R.id.priorityTextView);
			editButton = (ImageButton) findViewById(R.id.editButton);
			editButton.setOnClickListener(this);
			discardButton = (ImageButton) findViewById(R.id.discardButton);
			discardButton.setOnClickListener(this);
			Log.d("demo", "Got Task " + task.getTaskId() + " from MainActivity");
			populateTaskToView(task);
		}
	}
	
	private void populateTaskToView(Task task) {
		title.setText(task.getTitle());
		date.setText(task.getFormatedDate());
		time.setText(task.getFormatedTime());
		priority.setText(task.getPriority());
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.editButton) {
			Intent intent = new Intent(this, EditActivity.class);
			intent.putExtra(TASK, task);
			Log.d("demo", "Sending task " + task.getTaskId() + " to EditActivity");
			startActivityForResult(intent, UPDATE_TASK);
		} else if (v.getId() == R.id.discardButton) {
			Intent intent = new Intent();
			intent.putExtra(TASK, task);
			intent.putExtra(ACTION, ACTION_DISCARD);
			setResult(RESULT_OK, intent);
			finish();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK) {
			if(requestCode == UPDATE_TASK) {
				
				Task newTask = (Task) data.getExtras().get(TASK);
				Intent intent = new Intent();
				intent.putExtra(TASK, newTask);
				intent.putExtra(OLD_TASK, taskToDelete);
				intent.putExtra(ACTION, ACTION_UPDATE);
				setResult(RESULT_OK, intent);
				populateTaskToView(newTask);
				task = newTask;
				Log.d("demo", "Received task " + task.getTaskId() + " from EditActivity");
			}
		}
	}
}
