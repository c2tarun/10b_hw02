package com.example.hw02;

import static com.example.hw02.Constants.DATE_FORMAT;
import static com.example.hw02.Constants.TIME_FORMAT;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * Task.java
 */

public class Task implements Parcelable {

	private String title;
	private Calendar date;
	private Calendar time;
	private String priority;
	private int taskId;

	public static int TASK_ID_COUNTER = 0;

	public Task() {

	}

	public Task(String title, Calendar date, Calendar time, String priority) {
		super();
		this.title = title;
		this.date = date;
		this.time = time;
		this.priority = priority;
		this.taskId = ++TASK_ID_COUNTER;
	}
	
	public String getFormatedDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(date.getTime());
	}
	
	public String getFormatedTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
		return sdf.format(time.getTime());
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(title);
		dest.writeLong(date.getTimeInMillis());
		dest.writeLong(time.getTimeInMillis());
		dest.writeString(priority);
		dest.writeInt(taskId);
	}
	
	public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>() {
		public Task createFromParcel(Parcel in) {
			return new Task(in);
		}

		public Task[] newArray(int size) {
			return new Task[size];
		}
	};

	private Task(Parcel in) {
		this.title = in.readString();
		
		Calendar dateInstance = Calendar.getInstance();
		dateInstance.setTimeInMillis(in.readLong());
		this.date = dateInstance;
		
		Calendar timeInstance = Calendar.getInstance();
		timeInstance.setTimeInMillis(in.readLong());
		this.time = timeInstance;
		
		this.priority = in.readString();
		this.taskId = in.readInt();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + taskId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (taskId != other.taskId)
			return false;
		return true;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public Calendar getTime() {
		return time;
	}

	public void setTime(Calendar time) {
		this.time = time;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public static int getTASK_ID_COUNTER() {
		return TASK_ID_COUNTER;
	}

	public static void setTASK_ID_COUNTER(int tASK_ID_COUNTER) {
		TASK_ID_COUNTER = tASK_ID_COUNTER;
	}


}
