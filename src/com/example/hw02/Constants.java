package com.example.hw02;

/**
 * Group10B_HW02
 *
 * Tarun Kumar Mall
 * Jeremy Cass
 * Pragya Rai
 *
 * Constants.java
 */
 
public class Constants {
	public static final int CREATE_TASK = 1;
	public static final int DISPLAY_TASK = 2;
	public static final int UPDATE_TASK = 3;
	//public static final String NEW_TASK = "New Task";
	public static final int CURRENT_TASK = 11;
	
	//public static final String GET_TASK = "Get Task";
	public static final String TASK = "Task";
	public static final String OLD_TASK = "Old Task";
	
	public static final String ACTION = "Action";
	public static final String ACTION_CREATE = "Create Action";
	public static final String ACTION_DISCARD = "Discard Action";
	public static final String ACTION_UPDATE = "Update Action";
	
	public static final String TIME_FORMAT = "hh:mm a";
	public static final String DATE_FORMAT = "yyyy/MM/dd";
}
